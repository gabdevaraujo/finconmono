package com.guanabaras.fincontrol.repository.lancamento;

import com.guanabaras.fincontrol.models.lancamento.Lancamento;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LancamentoRepository extends JpaRepository<Lancamento, Long>{
    
}
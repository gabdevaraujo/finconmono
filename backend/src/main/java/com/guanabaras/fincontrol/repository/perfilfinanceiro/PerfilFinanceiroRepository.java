package com.guanabaras.fincontrol.repository.perfilfinanceiro;

import com.guanabaras.fincontrol.models.perfilfinanceiro.PerfilFinanceiro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerfilFinanceiroRepository extends JpaRepository<PerfilFinanceiro, Long>{
    
}
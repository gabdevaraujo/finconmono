package com.guanabaras.fincontrol.repository.usuario;

import java.util.Optional;

import com.guanabaras.fincontrol.models.usuario.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    
    Optional<Usuario> findByUsername(String username);
}
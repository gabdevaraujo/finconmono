package com.guanabaras.fincontrol.models.lancamento.enums;

import lombok.Getter;

public enum TipoLancamento {
    
    ENTRADA(1, "Entrada"),
    SAIDA(2, "Saída");

    @Getter private Integer id;
    @Getter private String descricao;

    TipoLancamento(Integer id, String descricao){
        this.id = id;
        this.descricao = descricao;
    }
}
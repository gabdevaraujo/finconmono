package com.guanabaras.fincontrol.models.usuario.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class UsuarioDTO {

    private Long id;
    private String username;
    private String telefone;
    private String nomeCompleto;
    
}
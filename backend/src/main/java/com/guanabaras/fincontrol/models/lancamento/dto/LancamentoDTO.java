package com.guanabaras.fincontrol.models.lancamento.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.guanabaras.fincontrol.models.usuario.Usuario;
import com.guanabaras.fincontrol.models.usuario.dto.UsuarioDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LancamentoDTO {
    
    private Long id;
    private LocalDateTime dataHora;
    private BigDecimal valor;
    private String tipo;
    private String detalhes;
    private UsuarioDTO usuario;
    
    public LancamentoDTO() {
    }

    public LancamentoDTO(Long id, LocalDateTime dataHora, BigDecimal valor, String tipo, String detalhes,
            Usuario usuario) {
        this.id = id;
        this.dataHora = dataHora;
        this.valor = valor;
        this.tipo = tipo;
        this.detalhes = detalhes;
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        if(usuario != null){
            usuarioDTO.setId(usuario.getId());
            usuarioDTO.setNomeCompleto(usuario.getNomeCompleto());
            usuarioDTO.setTelefone(usuario.getTelefone());
            usuarioDTO.setUsername(usuario.getUsername());
        }
        this.usuario = usuarioDTO;
    }

    
}
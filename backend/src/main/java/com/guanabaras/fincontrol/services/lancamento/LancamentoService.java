package com.guanabaras.fincontrol.services.lancamento;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.guanabaras.fincontrol.models.lancamento.Lancamento;
import com.guanabaras.fincontrol.models.lancamento.dto.LancamentoDTO;
import com.guanabaras.fincontrol.models.lancamento.enums.TipoLancamento;
import com.guanabaras.fincontrol.models.usuario.Usuario;
import com.guanabaras.fincontrol.programa.exceptions.ObjectNotFoundException;
import com.guanabaras.fincontrol.repository.lancamento.LancamentoRepository;
import com.guanabaras.fincontrol.repository.usuario.UsuarioRepository;
import com.guanabaras.fincontrol.resources.lancamento.LancamentoResource;
import com.guanabaras.fincontrol.services.lancamento.utils.LancamentoMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LancamentoService {

    Logger log = LoggerFactory.getLogger(LancamentoResource.class);
    
    @Autowired LancamentoRepository lancamentoRepository;

    @Autowired UsuarioRepository usuarioRepository;

    @PersistenceContext
    EntityManager em;

    public Lancamento findById(Long id){
        Optional<Lancamento> lancamento = lancamentoRepository.findById(id);
        return lancamento.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
    }

    public List<Lancamento> findAll(){
        return lancamentoRepository.findAll();
    }

    public void save(LancamentoDTO dto){
        Lancamento lancamento = LancamentoMapper.MAPPER.lancamentoDtoToLancamento(dto);
        lancamento = lancamentoRepository.save(lancamento);
        System.out.println(lancamento);
    }
    
    
    public void alimenta(){
        
        Lancamento lancamento = new Lancamento();
        Usuario usuario = new Usuario();
        usuario.setNomeCompleto("Gabriel Viana");
        usuario.setSenha("123456");
        usuario.setTelefone("$2a$10$CJOImHqsa7sURa99oomxMOi8/7USkC36miYS.WkNl3RCz4dHz2XYS");
        usuario.setUsername("gvianaraujo");

        usuarioRepository.save(usuario);
        
        lancamento.setDataHora(LocalDateTime.now());
        lancamento.setTipo(TipoLancamento.SAIDA);
        lancamento.setValor(BigDecimal.valueOf(50.00));
        lancamento.setDetalhes("Lanche");
        lancamento.setUsuario(usuario);
        
        lancamentoRepository.save(lancamento);       
        
        usuario.setLancamentos(new ArrayList<>());
        usuario.getLancamentos().add(lancamento);

        Lancamento lancamento1 = new Lancamento(); 
        
        lancamento1.setDataHora(LocalDateTime.now());
        lancamento1.setTipo(TipoLancamento.SAIDA);        
        lancamento1.setValor(BigDecimal.valueOf(50.00));
        lancamento1.setDetalhes("Almoço");
        lancamento1.setUsuario(usuario);
        lancamentoRepository.save(lancamento1);        
        
        usuario.getLancamentos().add(lancamento1);
        
        
    }    
}
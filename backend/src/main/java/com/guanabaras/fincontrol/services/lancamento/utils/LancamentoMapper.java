package com.guanabaras.fincontrol.services.lancamento.utils;

import com.guanabaras.fincontrol.models.lancamento.Lancamento;
import com.guanabaras.fincontrol.models.lancamento.dto.LancamentoDTO;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LancamentoMapper {

    LancamentoMapper INSTANCE = Mappers.getMapper( LancamentoMapper.class );

    LancamentoMapper MAPPER = Mappers.getMapper( LancamentoMapper.class );

    LancamentoDTO lancamentoToLancamentoDTO(Lancamento lancamento);

    Lancamento lancamentoDtoToLancamento(LancamentoDTO lancamentoDTO);
    
}
package com.guanabaras.fincontrol.resources.perfilfinanceiro;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.guanabaras.fincontrol.models.perfilfinanceiro.PerfilFinanceiro;
import com.guanabaras.fincontrol.repository.perfilfinanceiro.PerfilFinanceiroRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/perfilfinanceiro")
public class PerfilFinanceiroResource {

    @Autowired
    PerfilFinanceiroRepository repository;

    @GetMapping
    public ResponseEntity<List<PerfilFinanceiro>> getAll() {
        try {
            List<PerfilFinanceiro> items = new ArrayList<PerfilFinanceiro>();

            repository.findAll().forEach(items::add);

            if (items.isEmpty())
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);

            return new ResponseEntity<>(items, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<PerfilFinanceiro> getById(@PathVariable("id") Long id) {
        Optional<PerfilFinanceiro> existingItemOptional = repository.findById(id);

        if (existingItemOptional.isPresent()) {
            return new ResponseEntity<>(existingItemOptional.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<PerfilFinanceiro> create(@RequestBody PerfilFinanceiro item) {
        try {
            PerfilFinanceiro savedItem = repository.save(item);
            return new ResponseEntity<>(savedItem, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<PerfilFinanceiro> update(@PathVariable("id") Long id, @RequestBody PerfilFinanceiro item) {
        Optional<PerfilFinanceiro> existingItemOptional = repository.findById(id);
        if (existingItemOptional.isPresent()) {
            PerfilFinanceiro existingItem = existingItemOptional.get();
            System.out.println("TODO for developer - update logic is unique to entity and must be implemented manually.");
            //existingItem.setSomeField(item.getSomeField());
            return new ResponseEntity<>(repository.save(existingItem), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") Long id) {
        try {
            repository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}


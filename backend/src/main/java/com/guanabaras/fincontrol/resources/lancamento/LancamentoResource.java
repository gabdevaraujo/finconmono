package com.guanabaras.fincontrol.resources.lancamento;

import java.net.URI;
import java.util.List;

import com.guanabaras.fincontrol.models.lancamento.Lancamento;
import com.guanabaras.fincontrol.models.lancamento.dto.LancamentoDTO;
import com.guanabaras.fincontrol.repository.lancamento.LancamentoRepository;
import com.guanabaras.fincontrol.services.lancamento.LancamentoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/lancamentos")
public class LancamentoResource {

    Logger log = LoggerFactory.getLogger(LancamentoResource.class);

    @Autowired LancamentoRepository repository;

    @Autowired LancamentoService service;
    
    @GetMapping
    public ResponseEntity<List<Lancamento>> listaTodos(){
        service.alimenta();        
        List<Lancamento> lancamentos = service.findAll();        
        return ResponseEntity.ok(lancamentos);
    }

    @PostMapping
    public ResponseEntity<Lancamento> salvar(@RequestBody LancamentoDTO dto){
        service.save(dto);
        URI uri = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(dto.getId())
            .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Lancamento> findById(@PathVariable String id){
        service.alimenta();        
        Lancamento lancamento = service.findById(Long.valueOf(id));        
        return ResponseEntity.ok(lancamento);
    }
}